import { setFullMethodName } from "./utils"
import * as controllerAttributes from "./controller_attributes"
import { Context } from "koa";
import { IController } from "./controller";

// a decorator
// usage: @setAttr('auth', ...)
export function setAttr(type, opts){
  return function(target: Object, propertyKey: string, descriptor: TypedPropertyDescriptor<IController>) {
    const controller = descriptor.value

    // console.log('decorating', propertyKey, type, opts)
    
    controllerAttributes.setAttribute(controller, type, opts)
    
    // console.log({controller})

    return descriptor
  }
}

export function fullname(){
  return function(target: Object, propertyKey: string, descriptor?: TypedPropertyDescriptor<any>) {
    setFullMethodName(descriptor.value, target)
    return descriptor
  }
}
