import * as Koa from 'koa'
import * as Router from 'koa-router'

import authMiddleware, { authAttr as auth } from "./middleware/auth"
import {fullname} from "./decorators"

import { flatten_middleware, flatten } from "./middleware/flatten"
import Controller, { route, controller } from "./controller"
import { getAllAttributes } from "./controller_attributes"

var app = new Koa();
var router = new Router();

app.use(flatten_middleware)

app.use(async function global(ctx: Koa.Context, next) {
  console.log(ctx.method, ctx.href)
  await next()
  //console.log('global middleware end:', ctx.method, ctx.href, ctx.status)
  console.log(ctx.status, Controller.getControllerFromStack(ctx.middleware_chain).name)
})

router.use(authMiddleware.routes())

router.use(async function router(ctx, next){
  // console.log('middleware before', Array.from(ctx.app.middleware).forEach((...args)=>{
  //   console.log('  ->', ...args)
  // }))

  await next()

  //const flat_middleware = flatten(ctx.app, ctx)
  //console.log('middleware:', flat_middleware)
  //const ctrl = flat_middleware[flat_middleware.length-1]
  //console.log('controller:', ctrl.name)

  // console.log('middleware after', ctx.app.middleware)
})

function diagnostics(ctx: Koa.Context){
  return {
    request: `${ctx.method} ${ctx.path}`,
    middleware: ctx.middleware_chain.map(c => c.name),
    controller: Controller.getControllerFromStack(ctx.middleware_chain).name,
    attrs: getAllAttributes(ctx.middleware_chain),
  }
}

@controller
class MyDecoratedController {
  constructor() {
    // console.log("in MyDecoratedController constructor")
  }

  @route.get('/decorated')
  async myRoute(ctx, next){
    // console.log('in myRoute')
    ctx.body = diagnostics(ctx)
  }

  @route.post('/decorated')
  async myPostRoute(ctx, next){
    ctx.body = diagnostics(ctx)
  }
}

// must this method instead of instantiating MyDecoratedController directly for types to be configured correctly
const mdc = Controller.create(MyDecoratedController)
// const mdc = new MyDecoratedController() as Controller & MyDecoratedController
// console.log('mdc:', mdc)
// mdc.get('/path', mdc.myRoute)
//console.log(mdc.stack)

router.use(mdc.routes())

class MyController extends Controller {
  configureRoutes() {
    this.get('/', this.indexCtrl)
  }

  async indexCtrl(ctx, next) {
    // console.log('in class MyController:index' )
    ctx.body = diagnostics(ctx)
    // console.log('class.indexCtrl', ctx.body.middleware)
  }
}

class Router2 extends Controller {
  configureRoutes(){
    //this.get('/apikey', this.apikey)
    //this.get('/noauth', this.another_controller)
  //this.get('/admin', this.admin_controller)
    this.use(
      '/class',
      function intermediate(ctx, next){next()}, // add an intermediate controller so we can see it in the middleware list
      new MyController().routes()
    )
  }

  @route('get', '/apikey')
  @auth({no_auth_onbootstrap: true})
  apikey(ctx: Koa.Context, next){
    // console.log('in route another')
    ctx.body = diagnostics(ctx)
  }

  @route.get('/noauth')
  @fullname()
  @auth('skip')
  another_controller (ctx, next) {
    // console.log('in route another')
    ctx.body = diagnostics(ctx)
  }

  @route.get('/admin')
  @auth('admin')
  admin_controller (ctx, next) {
    // console.log('in route one')
    ctx.body = diagnostics(ctx)
  }
}
var router2 = new Router2()

router2.get('/normal',
  function controller_name(ctx: Koa.Context, next) {
    //console.log('in one')
    ctx.body = diagnostics(ctx)
    // console.log('normal', ctx.body.middleware)

  });

router.use('/top', router2.routes())

app
  .use(router.routes())
  // .use(router.allowedMethods());

if (require.main == module) {
  console.log(router.stack.map(i => i.path));
  const server = app.listen(3000, (...args)=>{
    console.log('listening on', (server.address() as any).port)
  });
}

export default app