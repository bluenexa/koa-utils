// evaluated at method definition time
function decorator(x: any){
  console.log('decorator created with', x)

  return function(target: Object, propertyKey: string, descriptor?: TypedPropertyDescriptor<any>) {
    // acts on actual decorated method
    console.log('decorator for', {target, propertyKey, descriptor})
  }
}

function auth(attrs: {}): MethodDecorator {
  return function(target: Object, propertyKey: string, descriptor?: TypedPropertyDescriptor<any>) {
    descriptor.value.__auth = attrs
  }
}

function override(){
  return function(target: Object, propertyKey: string, descriptor?: TypedPropertyDescriptor<any>) {
    descriptor.value = function(){
      console.log('here instead')
      console.log(target, propertyKey, descriptor)

      debugger
      
    }
  }
}

function fullname(){
  return function(target: Object, propertyKey: string, descriptor?: TypedPropertyDescriptor<any>) {
    if(!~propertyKey.indexOf('.')){
      const name = `${target.constructor.name}.${propertyKey}`
      Object.defineProperty(descriptor.value, "name", { value: name })
    }
    return descriptor
  }
}


export function autolog(rich_context: boolean = false){
  return function(target: Object, propertyKey: string, descriptor?: TypedPropertyDescriptor<any>) {
    const originalMethod = descriptor.value
    
    descriptor.value = function(...args: any[]){
      try {
        console.log('\n[AUTOLOG] executing:', originalMethod.name, {rich_context})
        return originalMethod.apply(this, args)
      } catch(e){
        // add info about class/method where this exception was thrown
        if(rich_context){
          e.context = {
            // class: target.constructor.name,
            // method: propertyKey,
            name: originalMethod.name,
          }
        }

        throw e
      } finally {
        console.log('[AUTOLOG] done:', originalMethod.name, '\n')
      }
    }

    // maintain all properties of method
    Object.defineProperties(descriptor.value, Object.getOwnPropertyDescriptors(originalMethod))

    return descriptor
  }
}


class Test {
  prop: string

  @decorator('world')
  myfunc(msg: string){
    console.log(msg)
  }
  
  @auth({skip: true})
  greeting(msg: string){
    console.log(msg)
  }

  @override()
  overrideme(msg: string){
    console.log('should not print', msg)
  }

  @autolog() // rich_context: false
  @fullname()
  throwserror(){
    throw new Error('boom')
  }

  @autolog(true)
  @fullname()
  throwserror2(){
    console.log('this', this)
    throw new Error('boom2')
  }
  
}

console.log('=============== runtime ===============')

const t = new Test()
t.prop = 'value'

t.myfunc('hi')
t.myfunc('hello')

t.overrideme('hello')

console.log(t.greeting, t.greeting['__auth']) // using property accessor to get past TS type checking of .__auth

try {
  t.throwserror()
} catch(e){
  console.log('Error', JSON.stringify(Object.assign({message: e.message}, e)))
}
try {
  t.throwserror2()
} catch(e){
  console.log('Error', JSON.stringify(Object.assign({message: e.message}, e)))
}
console.log('t', t)

console.log(t.throwserror)
console.log(t.throwserror2)
