import { IController } from "./controller"

export const ATTRS = Symbol("attrs")

// shape of attribute in the controller attrs[type] array
export type ControllerAttribute = string | object

// add an attrs symbol to the IController interface - {type: [...attributes]}
export interface IController {
//  [ATTRS]: {[propName: string]: ControllerAttribute[]}
}


export function setAttribute(
  controller: IController, type: string, value: ControllerAttribute
): void {
  const attributes = getAttributes(controller, type)
  // ensure that we have no duplicates
  controller[ATTRS][type] = [...new Set([...attributes, value])]
}

export function getAttributes(controller: IController, type: string): Array<ControllerAttribute> {
  return getAttributesForType(controller, type)
}

// flattens all attributes of specified type from an array of controllers/middleware stack
export function getAllAttributesForType(controllers: IController[], type: string): Array<ControllerAttribute> {
  const attributes = controllers
    .filter( c => c[ATTRS] && c[ATTRS][type] )
    .map( c => c[ATTRS][type] )
    .reduce( (acc, curr) => acc.concat(curr), [] ) // flatten the arrays from each controller

  return attributes
}

export function getAllAttributes(controllers: IController[]): Array<ControllerAttribute> {
  const attributes = controllers
    .filter( c => c[ATTRS] )
    .map( c => {return {[c.name]: c[ATTRS]}} )
    .reduce( (acc, curr) => acc.concat(curr), [] ) // flatten the arrays from each controller

  return attributes
}


// ensures that dest[key][type] = [] exists as an array and returns it
function getAttributesForType(controller: IController, type: string) : ControllerAttribute[] {
  controller[ATTRS] = controller[ATTRS] || {}
  controller[ATTRS][type] = controller[ATTRS][type] || []
  return controller[ATTRS][type]
}
