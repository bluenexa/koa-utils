import Controller from "../controller"
import * as controllerAttributes from "../controller_attributes"
import {setAttr} from "../decorators"

export const AUTH_KEY = 'auth'

export class AuthController extends Controller {
  configureRoutes() {
    this.use(this.mw)
  }

  mw(ctx, next){
//    console.log('===> auth middleware', 'before')

    // check down the middleware stream for auth instructions (does it have to be just downstream?)
    // const my_index = ctx.middleware_chain.indexOf(this.mw)
    // const downstream_middleware = ctx.middleware_chain.slice(my_index + 1)
    // // console.log('downstream', downstream_middleware)

    const downstream_middleware = ctx.middleware_chain

    const auth_instructions = controllerAttributes.getAllAttributesForType(
      downstream_middleware, AUTH_KEY
    )
    // console.log('auth_instructions:', auth_instructions)

    if(auth_instructions.length === 0){
      console.log('+++++ > performing normal auth check')
    }
    else {
      for(let instruction of auth_instructions){
        if(instruction === 'skip'){
          console.log('++++++ > skipping auth!')
        }
        if(instruction === 'admin'){
          console.log('++++++ > ADMIN CREDENTIALS required')
        }
      }
    }
    
    let result = next(ctx)
    //console.log('===> auth middleware', 'after')
    return result
  }
}

// export the auth decorator
export const authAttr = setAttr.bind(this, AUTH_KEY) // partial application

export default new AuthController()
