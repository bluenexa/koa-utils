import { IController } from "../controller";
import { Context } from "koa";

// add .flattened_middleware to the Context type
// export interface IContext {
//  middleware_chain: IController[]
// }

declare module "koa" {
    /**
     * See https://www.typescriptlang.org/docs/handbook/declaration-merging.html for
     * more on declaration merging
     *
     * It is optinal so that it's compatible with the original Koa.Context and can be passed to routers
     */
    export interface Context {
      middleware_chain?: IController[]
    }
}

// usage: flatten(ctx.app, ctx)
export const flatten = function(nested_chain: any, ctx: {path: string, method: string}): IController[]{
  var middleware = []
  // console.log('nested_chain:', nested_chain)

  let more_middlewares = []

  if(nested_chain.middleware){
    more_middlewares = nested_chain.middleware
  }
  else if(nested_chain.router){
    more_middlewares = nested_chain.router.match(ctx.path, ctx.method).pathAndMethod
  }
  else if(nested_chain.stack){
    more_middlewares = nested_chain.stack
  }
  else {
    middleware.push(nested_chain)
  }

  for(let m of more_middlewares){
    if(m.middleware || m.router || m.stack){
      middleware = middleware.concat(flatten(m, ctx))
    } else {
      middleware.push(m)
    }
  }
  
  return middleware
}


// attaches a .middleware_chain attribute that returns the flattend middleware chain
export function flatten_middleware(ctx: Context, next){
  ctx.middleware_chain = flatten(ctx.app, ctx)
  next()
}
