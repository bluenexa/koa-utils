export function setFullMethodName(value: Function, instance: any): Function {
  if(value.name.indexOf('.') < 0){
    Object.defineProperty(value, "name", { 
      value: `${instance.name || instance.constructor.name}.${value.name}`
    })
  }
  return value
}
