import app from "../index"
import axios from "axios"
import { promisify } from "util"

const serverStarted = (server)=>{
  return new Promise(resolve => server.on('listening', resolve))
}

describe("integration tests", () => {
  let server, client
  
  before('set up server', async () => {
    server = app.listen(0)
    // doesn't seem to be necesssary:
    //await serverStarted(server) 
    console.log('listening on port', server.address().port)

    client = axios.create({
      baseURL: `http://localhost:${server.address().port}`,
      timeout: 1000
    });
  })

  after(()=>{
    server.close()
  })

  const genericMw = [
    "flatten_middleware",
    "global",
    "AuthController.mw",
    "router",
  ]
  
  describe('api calls', () => {
    it('/class', async () => {
      const result = await client.get('/top/class')
      app.middleware.should.have.length(3)
      // console.log(result.status, result.data)
      result.data.should.eql({
        request: 'GET /top/class',
        controller: "MyController.indexCtrl",
        middleware: [
          ...genericMw,
          "Router2.intermediate",
          "MyController.indexCtrl",
        ],
        attrs: []
      })
    })

    it('/normal', async () => {
      const result = await client.get('/top/normal')
      // console.log(result.status, result.data)
      result.data.should.eql({
        request: 'GET /top/normal',
        controller: "Router2.controller_name",
        middleware: [
          ...genericMw,
          "Router2.controller_name",
        ],
        attrs: []
      })
    })


    it('/apikey', async () => {
      const result = await client.get('/top/apikey')
      // console.log(result.status, result.data)
      result.data.should.eql({
        request: 'GET /top/apikey',
        controller: "Router2.apikey",
        middleware: [
          ...genericMw,
          "Router2.apikey",
        ],
        attrs: [{
          'Router2.apikey': {
            auth: [{ 'no_auth_onbootstrap': true }]
          }
        }]
      })
    })

    it('/noauth', async () => {
      const result = await client.get('/top/noauth')
      // console.log(result.status, result.data)
      result.data.should.eql({
        request: 'GET /top/noauth',
        controller: "Router2.another_controller",
        middleware: [
          ...genericMw,
          "Router2.another_controller",
        ],
        attrs: [{
          'Router2.another_controller': {
            auth: ['skip']
          }
        }]
      })
    })

    it('/admin', async () => {
      const result = await client.get('/top/admin')
      // console.log(result.status, result.data)
      result.data.should.eql({
        request: 'GET /top/admin',
        controller: "Router2.admin_controller",
        middleware: [
          ...genericMw,
          "Router2.admin_controller",
        ],
        attrs: [{
          'Router2.admin_controller': {
            auth: ['admin']
          }
        }]
      })
    })

    it('GET /decorated', async () => {
      const result = await client.get('/decorated')
      // console.log(result.status, result.data)
      result.data.should.eql({
        request: 'GET /decorated',
        controller: "MyDecoratedController.myRoute",
        middleware: [
          ...genericMw,
          "MyDecoratedController.myRoute",
        ],
        attrs: []
      })
    })

    it('POST /decorated', async () => {
      const result = await client.post('/decorated')
      // console.log(result.status, result.data)
      result.data.should.eql({
        request: 'POST /decorated',
        controller: "MyDecoratedController.myPostRoute",
        middleware: [
          ...genericMw,
          "MyDecoratedController.myPostRoute",
        ],
        attrs: []
      })
    })

  })
})