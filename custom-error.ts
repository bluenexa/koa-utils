export default class CustomError extends Error {
  constructor(message) {
    super(message);

    // Maintains proper stack trace for where our error was thrown (only available on V8), see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, this.constructor);
    }

    if(!this.name || this.name == "Error"){
      this.name = this.constructor.name;
    }
  }
}
