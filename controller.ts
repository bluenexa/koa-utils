import * as Router from "koa-router"
import {setFullMethodName} from "./utils"
import { Middleware } from "koa";

import * as methods from "methods"

export interface IController extends Middleware {
  //name: string   /* name already exists on Middleware */
}

export const decoratedControllers = Symbol('decoratedControllers')

export default class Controller extends Router {
  name: String
  [decoratedControllers]: {method: string, path: string | RegExp, controller: IController}[]

  constructor(...args: any[]) {
    super()
    this.name = this.constructor.name

    this.__registerDecoratedControllers()

    this.configureRoutes()
  }

  // called by constructor auto register controllers specified by decorators
  __registerDecoratedControllers(){
    if(this[decoratedControllers]){
      this[decoratedControllers].forEach((route) => {
        this[route.method](route.path, route.controller)
      })
      // these have already been processed so remove them from the queue
      this[decoratedControllers].length = 0
    }
  }
  
  __wrap(controller_function){
    // console.log('wrapping', controller_function.name)

    // override the object's name property to point to the full name, including class name
    setFullMethodName(controller_function, this)

    // simple approach: just return the annotated function
    // return controller_function

    // more complex approach: allows us to set context for the execution of the controller function,
    // by creating a new function and moving over all properties too
    // needed if the controller or middleware wants to reference itself
    const controller = Object.defineProperties((...args)=>{
      return controller_function.apply(this, args)
    }, Object.getOwnPropertyDescriptors(controller_function))

    return controller
  }

  //overide register method to add our wrapper for name annotation
  register(...args){
    const [,,middleware,] = args

    // middleware is now an array of functions
    for(let i in middleware){
      middleware[i] = this.__wrap(middleware[i])
    }
    return super.register.apply(this, args)
  }

  // override use method to add our wrapper for name annotation
  use(...args){
    for(let i in args){
      if(args[i] instanceof Function){ // middleware check
        args[i] = this.__wrap(args[i])
      }
    }
    return super.use.apply(this, args)
  }

  // override me!
  configureRoutes(){
  }

  static getControllerFromStack(middleware: IController[]): IController {
    return middleware[middleware.length-1]
  }

  static create<T>(c: {new(): T; }) {
    return new c() as Controller & T;
  }
}

// class decorator for controllers, to be used with @route.* decorators instead of extending Controller base class
export function controller<T extends {new(...args:any[]):{}}>(constructor:T) {
  // console.log('extending', constructor)
  let controller

  class Class extends constructor{
    constructor(...args: any[]){
      super(...args)

      // should the controller be exposed on 'this' too, or is being exposed via the proxy enough?
      controller = new Controller()
      controller.name = constructor.name

      const proxy = new Proxy(this, {
        get(target, name){
          //console.log('get', {name})
          if(name in target){ // searches through the prototype chain too
            // console.log(`  get: resolving ${String(name)} with target`)
            return target[name]
          }
          // console.log('  get: resolving with CONTROLLER')
          return controller[name]
        },
        // set(target, prop, value, receiver): boolean {
        //   console.log('set', {target, receiver})
        //   if(prop in target){ // searches through the prototype chain too
        //     return Reflect.set(target, prop, value)
        //   }

        //   return Reflect.set(controller, prop, value)
        // }
      })

      controller.__registerDecoratedControllers.apply(proxy)

      return proxy
    }
  }

  Object.defineProperty(Class, 'name', {value: constructor.name})
  return Class
}

/* method decorators */

// allow route.<whatever> to be type compatible
interface Route {
  (method: string, path: string | RegExp): any,
  [prop: string]: Function
}

export const route = <Route>function route(method: string, path: string | RegExp) {
  if (!method || !path) {
    throw new Error('method and path are required arguments')
  }

  // note that constructor hasn't been called yet
  return function (target: Controller, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) {
    // console.log('@route.' + method, 'on', target)
    const fn = method.toLocaleLowerCase()
    target[decoratedControllers] = target[decoratedControllers] || []
    target[decoratedControllers].push({method, path, controller: descriptor.value})
  }
}
// create a decorator for every method
for(const method of [...methods, 'del', 'all']){
  // using Function.bind because methods contains 'bind'
  route[method] = Function.bind.call(route, null, method)
}
